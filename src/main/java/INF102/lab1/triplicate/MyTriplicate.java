package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {

        Map<T, Integer> map = new HashMap<>();
        for (T t : list) {
            Integer n = map.get(t);

            if(n == null){
                map.put(t, 1);
            }else {
                n = n+1;
                if(n >= 3) return t;

                map.put(t, n);
            }
        }
        return null;
    }
    
}
